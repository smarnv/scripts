#!/usr/bin/env python3
import argparse
import subprocess


def strip_inline(patterns):
    print('Stripping trailing whitespaces from files:', patterns)

    p = subprocess.run(
            'sed -i \'s/[[:space:]]\+$//\' ' + ' '.join(patterns),
            stderr=subprocess.PIPE, stdout=subprocess.PIPE,
            shell=True)

    print(p.stderr, p.stdout, sep='\n')


def strip_recursively(patterns):
    print('Stripping trailing whitespaces recursively excluding:', patterns)

    exclude_pattern = '.*\(git\|vim\|zsh\|env\|' + '\|'.join(patterns) + '\).*'
    p = subprocess.run(
            'find . -type f -writable ! -regex \'' + exclude_pattern + '\'' +
            ' -execdir grep -Iq . {} \;' +
            ' -execdir sed -i \'s/[[:space:]]\+$//\' {} \+ -print',
            stderr=subprocess.PIPE, stdout=subprocess.PIPE,
            shell=True, universal_newlines=True)

    print(p.stderr, p.stdout, sep='\n')


parser = argparse.ArgumentParser(description='Strip trailing whitespaces.')
parser.add_argument('patterns', metavar='pattern', type=str, nargs='+',
                    help='a filename to operate over')
parser.add_argument('--recursive', dest='accumulate', action='store_const',
                    const=strip_recursively, default=strip_inline,
                    help='strip whitespaces recursively excluding patterns')

args = parser.parse_args()
args.accumulate(args.patterns)
