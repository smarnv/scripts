#!/bin/sh

prompt_continue () {
  echo
  [ "$1" != "" ] && echo $1

  while true; do
    read -p "Do you want to continue? [yes/NO] " choice
    choice=$(echo "$choice" | tr '[:upper:]' '[:lower:]')

    if [ "$choice" = "yes" ]; then
      break
    elif [ "$choice" = "" -o "$choice" = "no" ]; then
      exit
    fi
  done
}

if [ $# -ne 2 ]; then
  echo "Usage: ./do-create-swap-file.sh <path> <size>"
  echo "e.g.   ./do-create-swap-file.sh / 4G"
  exit
fi

# check the current swap usage on the system
echo "$ sudo swapon -s"
sudo swapon -s
echo "---------------------------"
echo "$ free -hm"
free -hm

prompt_continue ""

# parse the the command-line parameters
if $(echo $1 | grep -q '^/'); then
  SWAP_FILE=$(readlink -m "$1/.swapfile")
else
  SWAP_FILE=$(readlink -m "./$1/.swapfile")
fi

if [ ! -z "$2" ]; then
  SIZE=$(($(numfmt --from=iec $2) / 1024))
fi

prompt_continue "Creating a swap file $SWAP_FILE of size $SIZE KiB..."

# create the swap file at the desired location
sudo dd if=/dev/zero of=$SWAP_FILE bs=1024 count=$SIZE status=progress
sudo chmod 600 $SWAP_FILE
sudo mkswap $SWAP_FILE
sudo swapon $SWAP_FILE

# verity that everything is working as intended
echo "$ sudo swapon -s"
sudo swapon -s
echo "---------------------------"
echo "$ free -hm"
free -hm
