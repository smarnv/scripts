#!/bin/sh

# The usage of this script makes only sense on Debian-based systems. It removes
# *all* packages that have left only their configurations after uninstall.
dpkg -l | grep ^rc | cut -d ' ' -f3 | xargs sudo dpkg --purge
