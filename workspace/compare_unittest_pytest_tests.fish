#!/usr/bin/env fish
#
# Compare the number of `pytest`- and `unittest/django`-based tests in project.

function print_no_tests_on -a branch
  git checkout -q $branch
  echo "* `$branch` – pytest:" \
     (rg -tpy '^def test_' | wc -l)'; unittest:' \
     (rg -tpy '^ +def test_' | wc -l)
end

set get_filenames string replace -r '(.*):.*' '$1'
echo -n 'files using –  pytest:' (
  rg -wtpy 'import' -g !'conftest.py' | rg -w 'pytest' \
     | $get_filenames | sort -iu | wc -l
)

echo '; unittest:' (
  rg -wtpy 'import' | rg -w '(django.|unit)test' \
     | $get_filenames | sort -iu | wc -l
)
echo

set current_branch (git branch -a | string replace -fr '^\* ' '')
print_no_tests_on 'master'
print_no_tests_on $current_branch
