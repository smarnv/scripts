#!/bin/sh

# Gives your group members and others **read** access to current folder, its
# files and all of its subfolders, together with their files, recursively.
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
