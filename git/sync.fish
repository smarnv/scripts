#!/usr/bin/env fish
#
# git-sync - synchronise a forked git repository (and its `origin` remote) in
#            the current working directory with its `upstream` remote.

set current_path (dirname (realpath (pwd)))

if [ ! -d '.git' ]
  echo "$current_path is not a valid git repository. Program aborted!"
  exit 1
else
  echo "Working on a valid git repository in $current_path..."
end

function should_continue_or_exit
  while ! contains -- $answer '' 'y' 'n'
    read -n 1 -P "Do you want to continue? [Y/n] " answer
    set (string lower $answer) answer
  end
  [ $answer = 'n' ] && exit
end

function log -a message
  echo -e "\n$message"
end

log 'Removing local branches/tags different from `master`...'
git checkout master
git branch -d (string trim -- (git branch | grep -v '^\* '))
git tag -d (git tag -l)

log 'Fetching remote changes...'
git fetch --all
set remote_branches (string trim -- (git branch -r | grep -v 'master'))

log 'Removing branches/tags on `origin`...'
git push origin :(string replace -fr -- '^origin/' '' $remote_branches)
git push origin :(git tag -l)
git branch -a
should_continue_or_exit

log 'Setting local branches to track `upstream`...'
for branch in (string replace -fr -- '^upstream/' '' $remote_branches)
  git branch --track $branch upstream/$branch
end
should_continue_or_exit

log 'Merging changes from `upstream` to `master`...'
git pull upstream master
should_continue_or_exit

log 'Pushing merged `master` to `origin`...'
git push --mirror origin

log "Git fork in $current_path synchronized successfully!"

# vim: ft=fish
