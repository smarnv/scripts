#!/bin/sh

# This script finds subfolders of $2 with a lot of files (inodes) in them.

if [ $# -gt 2 ] || [ $# -lt 1 ]; then
	set -- "error" "error"
elif [ "$2" = "" ]; then  
	set -- "$1" "/"
fi

case $1 in
	"info")
		df -ih
		;;
	"normal")
		echo "Working on folder $2"
		cd $2
		echo 'echo $(ls -a "$1" | wc -l) $1' >/tmp/count_em_$$
		chmod 700 /tmp/count_em_$$
		find . -mount -type d -print0 | xargs -0 -n1 /tmp/count_em_$$ | sort -n
		rm -f /tmp/count_em_$$
		;;
	"short")
		echo "Working on folder $2"
		cd $2
		for i in `ls -1A`; do echo "`find $i | sort -u | wc -l` $i"; done | sort -n
		;;
	"critical")
		## useful when there are *no* free inodes left
		echo "Working on folder $2"
		cd $2
		find . -xdev -type f | cut -d "/" -f 2 | sort | uniq -c | sort -n
		;;
	*)
		echo ""
		echo "There are 4 modes available: info, normal, short, critical."
		echo ""
		;;
esac

