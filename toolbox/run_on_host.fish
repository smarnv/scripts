#!/usr/bin/env fish

set executable (status basename)
exec flatpak-spawn --host $executable $argv
