#!/bin/sh
#
# Count occurrences in logs for canonical URLs of websites.
#
# Follow HTTP redirects and avoid using HEAD requests as some servers might not
# support them well. Issue GET requests and completely ignore the response body
# since it is not relevant for checking whether the target URL is reachable.
#
# The config file should use a Markdown-like format. The following are ignored:
#  - empty lines and leading whitespace
#  - hyphen (-) or star (*), surrounded by intervals, in the beginning of lines
#  - lines starting with `#`, `~~`, or `<!--` – for Markdown strike-through,
#    shell-style comments, or HTML-style `<!-- one-line comments -->`.

default_filename='./websites.md'

if [ "$1" = '--help' ] || [ "$1" = "-h" ]; then
  echo 'Usage: check_logs [--help|-h|file]'
  echo
  echo 'Count occurrences in logs for canonical URLs of websites.'
  echo "If \`file\` is not given, they are read from \`$default_filename\`."
  exit
fi

filename=$([ -n "$1" ] && echo "$1" || echo "$default_filename")
canonical_urls=$(./get_canonical_urls.sh $filename)

for url in $canonical_urls; do
  echo "$url $(zgrep $url /var/log/nginx/access.log* | wc -l)"
done | column -t
