#!/usr/bin/env python3

"""Command-line volume control: up, down, mute."""

import alsaaudio
import sys


def normalize(percentage):
    if percentage > 100:
        return 100
    if percentage < 0:
        return 0
    return percentage


def change_as_master(control_name, master_mute):
    control_mixer = alsaaudio.Mixer(control=control_name, cardindex=1)
    control_mixer.setmute(master_mute)
    control_mixer.close()

mixer = alsaaudio.Mixer(cardindex=1)
volume = mixer.getvolume()[0]
action = sys.argv[1].lower().strip()

if action == 'up':
    mixer.setvolume(normalize(volume+10))
elif action == 'down':
    mixer.setvolume(normalize(volume-10))
elif action == 'mute':
    master_mute = not mixer.getmute()[0]
    mixer.setmute(master_mute)
    change_as_master(u'Headphone', master_mute)
    change_as_master(u'Speaker', master_mute)

mixer.close()
