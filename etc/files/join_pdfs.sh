#!/bin/sh

if [ -x "/usr/bin/pdfjoin" ]; then
	pdfjoin "$@" --rotateoversize false --outfile finished.pdf
elif [ -x "/usr/bin/gs" ]; then
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=finished.pdf "$@"
else
	echo "Could not find a program to join files: $@"
fi
