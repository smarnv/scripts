#!/bin/sh

# Gives *only* your group members **read** access to current folder, its
# files and all of its subfolders, together with their files, recursively.
find . -type d -exec chmod 750 {} \;
find . -type f -exec chmod 640 {} \;
