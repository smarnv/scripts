#!/usr/bin/env fish

set filename (status filename)
set program_name (string split -rm 1 -- '/' $filename)[-1]

if ! distrobox list &> /dev/null
	# Assume execution in a distrobox container.
	set real_filename (realpath $filename)

	for program_bin in (realpath -q $PATH/$program_name | sort -iu)
		if [ -x "$program_bin" -a "$program_bin" != "$real_filename" ]
			exec $program_bin $argv
		end
	end

	exec echo 'Program not found in $PATH:' $program_name
end

switch $program_name(count $argv)
	case 'delta0'
		# A pager should go though the system pager.
		distrobox enter "bx_$DEFAULT_DISTROBOX" -- $program_name | $PAGER
	case 'exa*'
		# A file list program should use colours.
		exec distrobox enter "bx_$DEFAULT_DISTROBOX" -- $program_name '--colour=always' $argv
	case '*'
		exec distrobox enter "bx_$DEFAULT_DISTROBOX" -- $program_name $argv
end
