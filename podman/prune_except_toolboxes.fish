#!/usr/bin/env fish
#
# Prune system data related to Podman except for running containers and
# existing toolbox containers, regardless if currently running or not.


set script_location (dirname (realpath (status filename)))
cd $script_location

../toolbox/start_all.fish 2>&1 > /dev/null
podman system prune -af
../toolbox/stop_others.fish 2>&1 > /dev/null

function print_title -a title
  set sep (string repeat -n 42 '-')
  echo \n$sep "Remaining $title" $sep
end

print_title Pods
podman pod ps

print_title Containers
podman ps -a

print_title Toolboxes
toolbox list
