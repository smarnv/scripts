#!/usr/bin/env fish

test -n "$argv"; and set count $argv; or set count 10
history \
   | awk '{ CMD[$1]++; count++; } END { for (a in CMD) print CMD[a] " " CMD[a]/count*100 "% " a; }' \
   | grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n$count
