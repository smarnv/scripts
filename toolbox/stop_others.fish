#!/usr/bin/env fish

set env_file /run/.containerenv

if [ -f $env_file ]
  set current_container (
    awk -F '=' '/^name=/ { print $2 }' $env_file | tr -d \"
  )
  set podman_cmd flatpak-spawn --host podman
else
  set current_container 'host system'
  set podman_cmd podman
end

set containers_to_stop (
  toolbox list | awk 'p {print $2}; EOF{p=0} /CONTAINER NAME/{p=1}' \
     | grep -v $current_container
)

$podman_cmd stop $containers_to_stop &> /dev/null
toolbox list
