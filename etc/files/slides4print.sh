#!/bin/sh

if [ -x "/usr/bin/pdfjam" ]; then
	pdfjam "$@" --a4paper --landscape --frame true --nup 2x2 --delta "0.5cm 0.5cm" --scale 0.95 --outfile finished.pdf
else
	echo "Could not find a program to join files: $@"
fi
