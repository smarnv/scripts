#!/bin/sh

feh --no-fehbg --bg-scale --magick-timeout 0 ~/.local/default-wallpaper.jpg

pkill conky
conky -c ~/.conky/sysinfo.rc &

exit 0
