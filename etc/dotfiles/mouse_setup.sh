#!/usr/bin/env fish

set MOUSE (xinput list | grep -i 'Logitech' | cut -f 1 | cut -d" " -f 5- | sed 's/\s\+$//g')
xinput set-prop "$MOUSE" "libinput Accel Speed" 1
