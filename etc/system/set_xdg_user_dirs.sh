#!/bin/sh

xdg-user-dirs-update --set DESKTOP ~/Desktop
xdg-user-dirs-update --set DOCUMENTS ~/Documents
xdg-user-dirs-update --set DOWNLOAD ~/Downloads
xdg-user-dirs-update --set MUSIC ~/Music
xdg-user-dirs-update --set PICTURES ~/Pictures
xdg-user-dirs-update --set PUBLICSHARE ~/Public
xdg-user-dirs-update --set TEMPLATES ~/Templates
xdg-user-dirs-update --set VIDEOS ~/Videos
