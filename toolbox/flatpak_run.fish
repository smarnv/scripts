#!/usr/bin/env fish

set executable (status basename)
set matching_app_ids (
  flatpak-spawn --host flatpak list --app --columns=app | grep -i $executable
)

if [ (count $matching_app_ids) -ne 1 ]
  echo "There is no unique Flatpak app ID that matches `$executable`."
  echo 'Possible candidates are:' \n'- '$matching_app_ids
  exit 1
else
  set app_id $matching_app_ids
end

flatpak-spawn --host flatpak run $app_id $argv
