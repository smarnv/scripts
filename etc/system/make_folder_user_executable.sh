#!/bin/sh

# Gives *only* you **executable** access to current folder, its
# files and all of its subfolders, together with their files, recursively.
./make-folder-group-readable.sh
chmod u+x . -R
