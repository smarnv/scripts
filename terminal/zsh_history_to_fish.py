#!/usr/bin/env python3

import os
import re

expand_path = os.path.expanduser

def zsh_to_fish(cmd):
    return cmd.replace('&&', '; and ').replace('||', '; or ')


def is_valid_fish(cmd):
    for reg in r'^\S+=', r'\$\(', r'\[ ', r'`', r'\\$':
        if re.match(reg, cmd):
            return False
    return True


# you can prepend it to your original file manually
with open(expand_path('~/.local/share/fish/fish_history2'), 'wt') as o:
    with open(expand_path('~/.zsh_history'), 'rt', encoding='ISO-8859-1') as f:
        line_no = 0

        for line in f:
            line = line.strip()
            line_no += 1

            if line and re.match(r'^:\s+\d+:\d;', line):
                meta, command = line.split(';', 1)
                command = zsh_to_fish(command)

                if is_valid_fish(command):
                    time = meta.split(':')[1].strip()
                    o.write(f'- cmd: {command}\n   when: {time}\n')
                    print(f'{line_no}\t{command}')
