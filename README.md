# helper scripts

Little programs that may be useful at times.

## Usage

Copy or symlink the scripts to a location on your `$PATH`, e.g.

```sh
ln -s $(pwd)/base/read_confirm.fish $HOME/.local/bin/read_confirm
```

## License

Unless explicitly stated otherwise in the respective script, the general terms
and conditions from the LICENSE file apply.
