#!/bin/sh

ENTRY_POINT="$PWD"
[[ -e .mrconfig ]] || { echo "Run in a folder containing a local .mrconfig file."; exit; }

# git submodule check
echo "[$(date -Iseconds)] Start of git submodule check."
cd $ENTRY_POINT

echo "---------------------------"
for folder in $(find -maxdepth 4 -type d -name ".git" | sed 's/\.\/\(.*\)\/.git/\1/'); do
  cd $ENTRY_POINT/$folder
  git submodule --quiet sync --recursive
  git submodule update --init --recursive
done

echo "---------------------------"
echo "[$(date -Iseconds)] End of git submodule check."

# .mrconfig check
echo "[$(date -Iseconds)] Start of .mrconfig check."
cd $ENTRY_POINT
available=$(find * -type d -name .git -o -name .hg | sed "s/\/\..\+//")
registered=$(grep '\[' .mrconfig | tr -d "[]")

echo "---------------------------"
echo "> Available: $(echo "$available" | wc -l). Checking for NOT registered repos:"
for repo in $available; do
	[[ "$registered" =~ "$repo" ]] || { echo "$repo"; }
done

echo "---------------------------"
echo "> Registered: $(echo "$registered" | wc -l). Checking for EXTRA registered repos:"
for repo in $registered; do
	[[ "$available" =~ "$repo" ]] || { echo "$repo"; }
done

echo "---------------------------"
echo "[$(date -Iseconds)] End of .mrconfig check."
