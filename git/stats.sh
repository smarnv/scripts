#!/bin/sh

git log --author="$1" --pretty=tformat: --numstat \
  | perl -ane'$i += $F[0]; $d += $F[1]; END{ print "added: $i removed: $d\n"}'
