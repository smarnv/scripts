# Scripts for Working with Toolbox Containers

[Toolbox](https://github.com/containers/toolbox) is a tool for Linux operating
systems, which allows the use of containerised command-line environments. It is
built on top of [Podman](https://podman.io) and other standard container
technologies from the [Open Container Initiative](https://opencontainers.org).

[Distrobox](https://github.com/89luca89/distrobox) is a closely related toolbox
container manager that uses Podman or Docker to create containers that are
highly integrated with the container host system. It implements the same
concepts as Toolbox but using POSIX `sh` and aiming at broader compatibility.

## 1. Run programs in a toolbox container

Wrappers to explicitly call programs in a container:

* `run_in_distrobox` – for running programs in a distrobox container

### 1.1. Requirements

* distrobox containers: to be named in the format `bx_<name>`
* on host system:
	- `distrobox` (for managing distrobox containers)
	- the environment variable `$DEFAULT_DISTROBOX` to be set

### 1.2. Setup

1. Make sure that a container with the name `bx_$DEFAULT_DISTROBOX` exists.
2. On the host system, add somewhere to your `$PATH`, e.g. in `~/.local/bin`
    - the main wrapper script
    - symlinks to it with the names of the programs to run on the host system
	- see examples how to achieve something similar in section 2.2.

3. Now you can run these programs from the host system inside the container.
   The commands will be run in the distrobox container with the name above.

If the `distrobox` executable is not found, it is assumed that the wrapper
script is already executed in a `distrobox` container. The target command is
then run directly with all supplied command-line parameters.

## 2. Run programs on host system

Wrappers to call programs on the host system of a container:

* `run_on_host` – for running regular programs on the host system
* `flatpak_run` – for running Flatpak apps with an app ID that matches a
  certain pattern (ignoring letter case)

### 2.1. Requirements

* within containers: `flatpak-spawn` and `fish` shell
* on host system: `flatpak` (for running Flatpak apps)

### 2.2. Setup

1. Enter the container in which you want to install the shortcuts.
3. Add somewhere to your `$PATH`, e.g. in `/usr/local/bin`
    - the main wrapper script
        ```sh
        cp $(pwd)/run_on_host.fish /usr/local/bin/run_on_host
        # or
        cp $(pwd)/flatpak_run.fish /usr/local/bin/flatpak_run
        ```
    - symlinks to it with the names of the programs to run on the host system
        ```sh
        cd /usr/local/bin  # the path where the wrapper script is located
        # then
        ln -s run_on_host podman
        ln -s run_on_host flatpak
        ln -s run_on_host rpm-ostree
        ln -s run_on_host systemctl
        # or
        ln -s flatpak_run firefox
        ln -s flatpak_run gimp
        ln -s flatpak_run flatseal
        ln -s flatpak_run gitg
        ```
3. Now you are able to run these programs on the container host system from
   within the (toolbox) container.
